# NextCloud with web-based editor made easy
The purpose of this repository is to provide various docker-compose options to run NextCloud with web-based editor capabilities. 

2 options are availble so far:
- NextCloud + Collabora CODE
- NextCloud + LibreOffice Online

## Pre-requisites
### Install Docker
See https://docs.docker.com/install/

### Customize your /etc/hosts file
Add the following 2 lines into your /etc/hosts file.
``` 
127.0.0.1 appli
127.0.0.1 ooo
``` 

### Pick an option
- for NextCloud + Collabora CODE run 
``` $ cd nextcloud-docker-with-collabora ```
- for NextCloud + LibreOffice Online
``` $ cd nextcloud-docker-with-libreoffice ```

### Customize the .env
Create the .env file from the .env.sample:
``` $ cp .env.sample .env ```

## Launch and customize NextCloud
### Pull and start the containers
Run the following in the current folder (a docker-compose.yml file should be there):
``` $ docker-compose up ``` 

### Access the OpenOffice Online admin console
Access the OpenOffice Online admin console (refresh until it shows up):
http://ooo:9980/loleaflet/dist/admin/admin.html#

Logon with the admin credentials you specified into the .env file.

### Access the NextCloud UI
Access the NextCloud UI (refresh until it shows up):
http://appli/

Logon with the admin credentials you specified into the .env file.

### Install the Collabora Online app
Access the apps menu, search for Collabora Online app and click "Install and Enable".

Once installed, in the Settings menu, open the Collabora Online settings (it will be renamed LOOL after the configuration has completed if you are using LOOL as web-based documents editor option).
In the URL field enter "http://ooo:9980" and click Apply.

You can then edit OpenOffice documents directly into the NextCloud UI.

### Stopping the stack and restarting it as daemon
Hit Ctrl+C on the terminate to gracefully shutdown the containers.
Then start again the containers, this time in the background:
``` $ docker-compose up -d ```

## Thanks
All the meterial presented here is based on the following examples and documentation:
- https://github.com/smehrbrodt/nextcloud-libreoffice-online
- https://github.com/aaronSkar/docker/tree/master/.examples
- https://www.collaboraoffice.com/code/docker/
